#Requires -Version 2.0

<#
    Installs pre-requisities for CI/CD testing.
#>

# Write some information to the host so I can see what is going on
"Git version: $(git.exe --version)"
"PSVersion:   $($PSVersionTable.PSVersion)"
"PSBuild:     $($PSVersionTable.BuildVersion)"
"clr version: $($PSVersionTable.ClrVersion)"
"Host name:   $($Host.Name)"

# Install new package provider
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.208 -Force

# Trust new package provider
Set-PSRepository -Name PSGallery -InstallationPolicy Trusted

# Change the execution policy to allow everything to run without hindrance
Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope LocalMachine -Confirm:$false -Force

# Install required modules
Install-Module -Name PowerShellGet -Force
Install-Module -Name PS2EXE -Force
Install-Module -Name PSSQLite -Force

# Import required modules
Import-Module -Name PSSQLite

